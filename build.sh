TOOLCHAINDIR=$(pwd)/../ez80-none-elf/bin # This is the toolchain that is build using <https://bitbucket.org/cocoacrumbselectronics/ez80-llvm-toolchain/src/master/>
OPT=-O1

mkdir ./build > /dev/null 2>&1
mkdir ./list > /dev/null 2>&1

# Compiling main.c
$TOOLCHAINDIR/clang -target ez80-none-elf $OPT -Wa,-march=ez80+full -nostdinc main.c -c -o ./build/main.o

# Compiling minimal set of .asm files to setup the ez80L92 CPU
$TOOLCHAINDIR/ez80-none-elf-as -march=ez80+full \
    -a=./list/vectors16.lst \
    ./crt/src/boot/common/vectors16.asm \
    -o ./build/vectors16.o

$TOOLCHAINDIR/ez80-none-elf-as -march=ez80+full \
    -a=./list/init_params_l92.lst \
    ./crt/src/boot/eZ80L92/init_params_l92.asm \
    -I ./crt/include/zilog \
    -o ./build/init_params_l92.o

$TOOLCHAINDIR/ez80-none-elf-as -march=ez80+full \
    -a=./list/cstartup.lst \
    ./crt/src/boot/common/cstartup.asm \
    -o ./build/cstartup.o

$TOOLCHAINDIR/ez80-none-elf-as -march=ez80+full \
    -a=./list/zsldevinitdummy.lst \
    ./crt/src/boot/common/zsldevinitdummy.asm \
    -o ./build/zsldevinitdummy.o

# Linking step
$TOOLCHAINDIR/ez80-none-elf-ld -T linkerScript.ld -Map=executable.map \
    ./build/vectors16.o \
    ./build/init_params_l92.o \
    ./build/cstartup.o \
    ./build/zsldevinitdummy.o \
    ./build/main.o \
    --oformat ihex \
    -o executable.hex
