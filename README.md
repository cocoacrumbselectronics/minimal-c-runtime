Minimal C runtime
=================

This repo contains the minimal amount of code to run this simple C code:

```C
int main(void)
{
	int	a;
	int	b;
	int	c;

	a = 1;
	b = 2;
	c = a + b;

	return c;
} /* end main */
```

It's meant as a simple test case for the [ez80-LLVM-toolchain](<https://bitbucket.org/cocoacrumbselectronics/ez80-llvm-toolchain/src/master/>). And uses the absolute minimum of assembly code to set up the eZ80L92 properly.

As it is, it assumes an eZ80L92 with the linker map set to 2 Mb RAM (starting at address 0x000000) and 8 Mb FLASH (not required for this simple case).

The result is an `executable.hex` file which can be loaded in the ZDS II IDE. You use the ZDS II simulator instead of real hardware to step through the code to verify the build.

The assembly code for the crt originates from the `ZDSII_eZ80Acclaim!_5.3.4` installation but needed small tweaks so that it could be compiled with the ez80-LLVM-toolchain.
