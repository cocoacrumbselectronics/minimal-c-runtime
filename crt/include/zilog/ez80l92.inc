;************************************************************************
;*    eZ80L92.inc
;*
;*		eZ80l92 Registers
;************************************************************************

;-------------------------------------------
; Clock and Power-Down

CLK_PPD1:	.equ	0xDB
CLK_PPD2:	.equ	0xDC

;-------------------------------------------
; General Purpose I/O

PB_DR:		.equ	0x9A
PB_DDR:		.equ	0x9B
PB_ALT1:	.equ	0x9C
PB_ALT2:	.equ	0x9D

PC_DR:		.equ	0x9E
PC_DDR:		.equ	0x9F
PC_ALT1:	.equ	0xA0
PC_ALT2:	.equ	0xA1

PD_DR:		.equ	0xA2
PD_DDR:		.equ	0xA3
PD_ALT1:	.equ	0xA4
PD_ALT2:	.equ	0xA5

;-------------------------------------------
; Chip Select / Wait State Generator

; Keept for Backward Compatability
CS_LBR0:	.equ	0xA8
CS_UBR0:	.equ	0xA9
CS_CTL0:	.equ	0xAA
CS_BMC0:	.equ	0xF0

CS_LBR1:	.equ	0xAB
CS_UBR1:	.equ	0xAC
CS_CTL1:	.equ	0xAD
CS_BMC1:	.equ	0xF1

CS_LBR2:	.equ	0xAE
CS_UBR2:	.equ	0xAF
CS_CTL2:	.equ	0xB0
CS_BMC2:	.equ	0xF2

CS_LBR3:	.equ	0xB1
CS_UBR3:	.equ	0xB2
CS_CTL3:	.equ	0xB3
CS_BMC3:	.equ	0xF3

; Manual Naming Convention
CS0_LBR:	.equ	0xA8
CS0_UBR:	.equ	0xA9
CS0_CTL:	.equ	0xAA
CS0_BMC:	.equ	0xF0

CS1_LBR:	.equ	0xAB
CS1_UBR:	.equ	0xAC
CS1_CTL:	.equ	0xAD
CS1_BMC:	.equ	0xF1

CS2_LBR:	.equ	0xAE
CS2_UBR:	.equ	0xAF
CS2_CTL:	.equ	0xB0
CS2_BMC:	.equ	0xF2

CS3_LBR:	.equ	0xB1
CS3_UBR:	.equ	0xB2
CS3_CTL:	.equ	0xB3
CS3_BMC:	.equ	0xF3

;-------------------------------------------
; Watch Dog Timer

WDT_CTL:	.equ	0x93
WDT_RR:		.equ	0x94

;-------------------------------------------
; Programmable Reload Timers

TMR_CTL0:	.equ	0x80
TMR_DRL0:	.equ	0x81
TMR_DRH0:	.equ	0x82
TMR_RRL0:	.equ	0x81
TMR_RRH0:	.equ	0x82

TMR_CTL1:	.equ	0x83
TMR_DRL1:	.equ	0x84
TMR_DRH1:	.equ	0x85
TMR_RRL1:	.equ	0x84
TMR_RRH1:	.equ	0x85

TMR_CTL2:	.equ	0x86
TMR_DRL2:	.equ	0x87
TMR_DRH2:	.equ	0x88
TMR_RRL2:	.equ	0x87
TMR_RRH2:	.equ	0x88

TMR_CTL3:	.equ	0x89
TMR_DRL3:	.equ	0x8A
TMR_DRH3:	.equ	0x8B
TMR_RRL3:	.equ	0x8A
TMR_RRH3:	.equ	0x8B

TMR_CTL4:	.equ	0x8C
TMR_DRL4:	.equ	0x8D
TMR_DRH4:	.equ	0x8E
TMR_RRL4:	.equ	0x8D
TMR_RRH4:	.equ	0x8E

TMR_CTL5:	.equ	0x8F
TMR_DRL5:	.equ	0x90
TMR_DRH5:	.equ	0x91
TMR_RRL5:	.equ	0x90
TMR_RRH5:	.equ	0x91

;-- Manual Naming

TMR0_CTL:	.equ	0x80
TMR0_DR_L:	.equ	0x81
TMR0_DR_H:	.equ	0x82
TMR0_RR_L:	.equ	0x81
TMR0_RR_H:	.equ	0x82

TMR1_CTL:	.equ	0x83
TMR1_DR_L:	.equ	0x84
TMR1_DR_H:	.equ	0x85
TMR1_RR_L:	.equ	0x84
TMR1_RR_H:	.equ	0x85

TMR2_CTL:	.equ	0x86
TMR2_DR_L:	.equ	0x87
TMR2_DR_H:	.equ	0x88
TMR2_RR_L:	.equ	0x87
TMR2_RR_H:	.equ	0x88

TMR3_CTL:	.equ	0x89
TMR3_DR_L:	.equ	0x8A
TMR3_DR_H:	.equ	0x8B
TMR3_RR_L:	.equ	0x8A
TMR3_RR_H:	.equ	0x8B

TMR4_CTL:	.equ	0x8C
TMR4_DR_L:	.equ	0x8D
TMR4_DR_H:	.equ	0x8E
TMR4_RR_L:	.equ	0x8D
TMR4_RR_H:	.equ	0x8E

TMR5_CTL:	.equ	0x8F
TMR5_DR_L:	.equ	0x90
TMR5_DR_H:	.equ	0x91
TMR5_RR_L:	.equ	0x90
TMR5_RR_H:	.equ	0x91

TMR_ISS:	.equ	0x92


;-------------------------------------------
; Real-Time Clock

RTC_SEC:	.equ	0xE0
RTC_MIN:	.equ	0xE1
RTC_HRS:	.equ	0xE2
RTC_DOW:	.equ	0xE3
RTC_DOM:	.equ	0xE4
RTC_MON:	.equ	0xE5
RTC_YR:		.equ	0xE6
RTC_CEN:	.equ	0xE7
RTC_ASEC:	.equ	0xE8
RTC_AMIN:	.equ	0xE9
RTC_AHRS:	.equ	0xEA
RTC_ADOW:	.equ	0xEB
RTC_CTRL:	.equ	0xED
RTC_ACTRL:	.equ	0xEC

;-------------------------------------------
; UARTs

UART_BRGL0:	.equ	0xC0
UART_BRGH0:	.equ	0xC1
UART_RBR0:	.equ	0xC0
UART_THR0:	.equ	0xC0
UART_IER0:	.equ	0xC1
UART_IIR0:	.equ	0xC2
UART_FCTL0:	.equ	0xC2
UART_LCTL0:	.equ	0xC3
UART_MCTL0:	.equ	0xC4
UART_LSR0:	.equ	0xC5
UART_MSR0:	.equ	0xC6
UART_SPR0:	.equ	0xC7

UART_BRGL1:	.equ	0xD0
UART_BRGH1:	.equ	0xD1
UART_RBR1:	.equ	0xD0
UART_THR1:	.equ	0xD0
UART_IER1:	.equ	0xD1
UART_IIR1:	.equ	0xD2
UART_FCTL1:	.equ	0xD2
UART_LCTL1:	.equ	0xD3
UART_MCTL1:	.equ	0xD4
UART_LSR1:	.equ	0xD5
UART_MSR1:	.equ	0xD6
UART_SPR1:	.equ	0xD7

;-- Correct Naming

UART0_BRG_L:	.equ	0xC0
UART0_BRG_H:	.equ	0xC1
UART0_RBR:	.equ	0xC0
UART0_THR:	.equ	0xC0
UART0_IER:	.equ	0xC1
UART0_IIR:	.equ	0xC2
UART0_FCTL:	.equ	0xC2
UART0_LCTL:	.equ	0xC3
UART0_MCTL:	.equ	0xC4
UART0_LSR:	.equ	0xC5
UART0_MSR:	.equ	0xC6
UART0_SPR:	.equ	0xC7

UART1_BRG_L:	.equ	0xD0
UART1_BRG_H:	.equ	0xD1
UART1_RBR:	.equ	0xD0
UART1_THR:	.equ	0xD0
UART1_IER:	.equ	0xD1
UART1_IIR:	.equ	0xD2
UART1_FCTL:	.equ	0xD2
UART1_LCTL:	.equ	0xD3
UART1_MCTL:	.equ	0xD4
UART1_LSR:	.equ	0xD5
UART1_MSR:	.equ	0xD6
UART1_SPR:	.equ	0xD7
;-------------------------------------------
; IrDA

IR_CTL:	.equ	0xBF

;-------------------------------------------
; SPI

SPI_BRGL:	.equ	0xB8
SPI_BRG_L:	.equ	0xB8
SPI_BRGH:	.equ	0xB9
SPI_BRG_H:	.equ	0xB9
SPI_CTL:	.equ	0xBA
SPI_SR:		.equ	0xBB
SPI_TSR:	.equ	0xBC
SPI_RBR:	.equ	0xBC

;-------------------------------------------
; I2C

I2C_SAR:	.equ	0xC8
I2C_XSAR:	.equ	0xC9
I2C_DR:		.equ	0xCA
I2C_CTL:	.equ	0xCB
I2C_SR:		.equ	0xCC
I2C_CCR:	.equ	0xCC
I2C_SRR:	.equ	0xCD

;-------------------------------------------


