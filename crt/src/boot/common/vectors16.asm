;*****************************************************************************
; vectors16.asm
;
; eZ80's Reset, RST and first generation interrupt vector arrangement
;*****************************************************************************
; Copyright (C) 2005 by ZiLOG, Inc.  All Rights Reserved.
;*****************************************************************************

;**************************************************************************
; Global Symbols Imported
;**************************************************************************

        XREF __init
        XREF __low_rom

;**************************************************************************
; Global Symbols Exported
;**************************************************************************

        XDEF _reset
        XDEF __default_nmi_handler
        XDEF __default_mi_handler
        XDEF __nvectors
        XDEF _init_default_vectors
        XDEF __init_default_vectors
        XDEF _set_vector
        XDEF __set_vector
        XDEF __2nd_jump_table
        XDEF __1st_jump_table
        XDEF __vector_table


NVECTORS EQU 48                ; number of interrupt vectors

; Save Interrupt State
.macro SAVEIMASK
    ld a, i                    ; sets parity bit to value of IEF2
    push af
    di                         ; disable interrupts while loading table 
    .endm

; Restore Interrupt State
.macro RESTOREIMASK
    pop af
    jp po, $+5                 ; parity bit is IEF2
    ei
    .endm


;*****************************************************************************
; Reset and all RST nn's
;  1. diaable interrupts
;  2. clear mixed memory mode (MADL) flag
;  3. jump to initialization procedure with jp.lil to set ADL
        ; DEFINE .RESET, SPACE = ROM
        ; SEGMENT .RESET
	.section	.reset, "ax", @progbits
	.assume	    adl = 1
	.file	    "vectors16.asm"
	.section	.reset, "ax", @progbits
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
_reset:
_rst0:
    di
    rsmix
    jp.lil __init
_rst8:
    di
    rsmix
    jp.lil __init
_rst10:
    di
    rsmix
    jp.lil __init
_rst18:
    di
    rsmix
    jp.lil __init
_rst20:
    di
    rsmix
    jp.lil __init
_rst28:
    di
    rsmix
    jp.lil __init
_rst30:
    di
    rsmix
    jp.lil __init
_rst38:
    di
    rsmix
    jp.lil __init
        ds 0x26
_nmi:
    jp.lil __default_nmi_handler



;*****************************************************************************
; Startup code
        ; DEFINE .STARTUP, SPACE = ROM
        ; SEGMENT .STARTUP
        ; .ASSUME ADL=1
	.section	.startup, "ax", @progbits
	.assume	    adl = 1
	.file	    "vectors16.asm"
	.section	.startup, "ax", @progbits
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; number of vectors supported
__nvectors:
        DW NVECTORS            ; extern unsigned short _num_vectors;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Default Non-Maskable Interrupt handler
__default_nmi_handler:
    retn

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Default Maskable Interrupt handler
__default_mi_handler:
    ei
    reti

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Initialize all potential interrupt vector locations with a known
; default handler.
;
; void _init_default_vectors(void);
__init_default_vectors:
_init_default_vectors:
    push af
    SAVEIMASK
    ld hl, __default_mi_handler
    ld a, 0xC3
    ld (__2nd_jump_table), a       ; place jp opcode
    ld (__2nd_jump_table + 1), hl  ; __default_hndlr
    ld hl, __2nd_jump_table
    ld de, __2nd_jump_table + 4
    ld bc, NVECTORS * 4 - 4
    ldir
    im 2                       ; Interrtup mode 2
    ld a, __vector_table >> 8
    ld i, a                    ; Load interrtup vector base
    RESTOREIMASK
    pop af
    ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Installs a user interrupt handler in the 2nd interrupt vector jump table
;
; void * _set_vector(unsigned int vector, void(*handler)(void));
__set_vector:
_set_vector:
    push iy
    ld iy, 0
    add iy, sp                 ; Standard prologue
    push af
    SAVEIMASK
    ld bc, 0                   ; clear bc
    ld b, 2                    ; calculate 2nd jump table offset
    ld c, (iy+6)               ; vector offset
    mlt bc                     ; bc is 2nd jp table offset
    ld hl, __2nd_jump_table
    add hl, bc                 ; hl is location of jp in 2nd jp table
    ld (hl), 0xC3               ; place jp opcode just in case
    inc hl                     ; hl is jp destination address
    ld bc, (iy+9)              ; bc is isr address
    ld de, (hl)                ; save previous handler
    ld (hl), bc                ; store new isr address
    push de
    pop hl                     ; return previous handler
    RESTOREIMASK
    pop af
    ld sp, iy                  ; standard epilogue
    pop iy
    ret


;*****************************************************************************
        ; DEFINE IVJMPTBL, SPACE = RAM
        ; SEGMENT IVJMPTBL
	.section	.ivjmptbl, "ax", @progbits
	.assume	    adl = 1
	.file	    "vectors16.asm"
	.section	.ivjmptbl, "ax", @progbits
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 2nd Interrupt Vector Jump Table
;  - this table must reside in RAM anywhere in the 16M byte range
;  - each 4-byte entry is a jump to an interrupt handler
__2nd_jump_table:
        DS NVECTORS * 4


;*****************************************************************************
; Interrupt Vector Table
;  - this segment must be aligned on a 256 byte boundary anywhere below
;    the 64K byte boundry
;  - each 2-byte entry is a 2-byte vector address
        ; DEFINE .IVECTS, SPACE = ROM, ALIGN = 100h
        ; SEGMENT .IVECTS
	.section	.ivects, "ax", @progbits
	.assume	    adl = 1
	.file	    "vectors16.asm"
	.section	.ivects, "ax", @progbits
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
__vector_table:
    dw __1st_jump_table + 0x00
    dw __1st_jump_table + 0x04
    dw __1st_jump_table + 0x08
    dw __1st_jump_table + 0x0c
    dw __1st_jump_table + 0x10
    dw __1st_jump_table + 0x14
    dw __1st_jump_table + 0x18
    dw __1st_jump_table + 0x1c
    dw __1st_jump_table + 0x20
    dw __1st_jump_table + 0x24
    dw __1st_jump_table + 0x28
    dw __1st_jump_table + 0x2c
    dw __1st_jump_table + 0x30
    dw __1st_jump_table + 0x34
    dw __1st_jump_table + 0x38
    dw __1st_jump_table + 0x3c
    dw __1st_jump_table + 0x40
    dw __1st_jump_table + 0x44
    dw __1st_jump_table + 0x48
    dw __1st_jump_table + 0x4c
    dw __1st_jump_table + 0x50
    dw __1st_jump_table + 0x54
    dw __1st_jump_table + 0x58
    dw __1st_jump_table + 0x5c
    dw __1st_jump_table + 0x60
    dw __1st_jump_table + 0x64
    dw __1st_jump_table + 0x68
    dw __1st_jump_table + 0x6c
    dw __1st_jump_table + 0x70
    dw __1st_jump_table + 0x74
    dw __1st_jump_table + 0x78
    dw __1st_jump_table + 0x7c
    dw __1st_jump_table + 0x80
    dw __1st_jump_table + 0x84
    dw __1st_jump_table + 0x88
    dw __1st_jump_table + 0x8c
    dw __1st_jump_table + 0x90
    dw __1st_jump_table + 0x94
    dw __1st_jump_table + 0x98
    dw __1st_jump_table + 0x9c
    dw __1st_jump_table + 0xa0
    dw __1st_jump_table + 0xa4
    dw __1st_jump_table + 0xa8
    dw __1st_jump_table + 0xac
    dw __1st_jump_table + 0xb0
    dw __1st_jump_table + 0xb4
    dw __1st_jump_table + 0xb8
    dw __1st_jump_table + 0xbc

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 1st Interrupt Vector Jump Table
;  - this table must reside in the first 64K bytes of memory
;  - each 4-byte entry is a jump to the 2nd jump table plus offset
__1st_jump_table:
    jp __2nd_jump_table + 0x00
    jp __2nd_jump_table + 0x04
    jp __2nd_jump_table + 0x08
    jp __2nd_jump_table + 0x0c
    jp __2nd_jump_table + 0x10
    jp __2nd_jump_table + 0x14
    jp __2nd_jump_table + 0x18
    jp __2nd_jump_table + 0x1c
    jp __2nd_jump_table + 0x20
    jp __2nd_jump_table + 0x24
    jp __2nd_jump_table + 0x28
    jp __2nd_jump_table + 0x2c
    jp __2nd_jump_table + 0x30
    jp __2nd_jump_table + 0x34
    jp __2nd_jump_table + 0x38
    jp __2nd_jump_table + 0x3c
    jp __2nd_jump_table + 0x40
    jp __2nd_jump_table + 0x44
    jp __2nd_jump_table + 0x48
    jp __2nd_jump_table + 0x4c
    jp __2nd_jump_table + 0x50
    jp __2nd_jump_table + 0x54
    jp __2nd_jump_table + 0x58
    jp __2nd_jump_table + 0x5c
    jp __2nd_jump_table + 0x60
    jp __2nd_jump_table + 0x64
    jp __2nd_jump_table + 0x68
    jp __2nd_jump_table + 0x6c
    jp __2nd_jump_table + 0x70
    jp __2nd_jump_table + 0x74
    jp __2nd_jump_table + 0x78
    jp __2nd_jump_table + 0x7c
    jp __2nd_jump_table + 0x80
    jp __2nd_jump_table + 0x84
    jp __2nd_jump_table + 0x88
    jp __2nd_jump_table + 0x8c
    jp __2nd_jump_table + 0x90
    jp __2nd_jump_table + 0x94
    jp __2nd_jump_table + 0x98
    jp __2nd_jump_table + 0x9c
    jp __2nd_jump_table + 0xa0
    jp __2nd_jump_table + 0xa4
    jp __2nd_jump_table + 0xa8
    jp __2nd_jump_table + 0xac
    jp __2nd_jump_table + 0xb0
    jp __2nd_jump_table + 0xb4
    jp __2nd_jump_table + 0xb8
    jp __2nd_jump_table + 0xbc

        END
