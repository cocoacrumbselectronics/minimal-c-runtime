;**************************************************************************
;  file zsldevinitdummy.asm
;  Dummy implementation file for opening peripheral devices.
;
;  Copyright (C) 1999-2004 by  ZiLOG, Inc.
;  All Rights Reserved.
;
;**************************************************************************

	; segment	CODE
	; .assume adl=1
	.section	.text, "ax", @progbits
	.assume	    adl = 1
	.file	    "zsldevinitdummy.asm"
	.section	.text, "ax", @progbits

;**************************************************************************
; Global Symbols Exported
;**************************************************************************

	xdef _open_periphdevice
	xdef __open_periphdevice
	xdef _close_periphdevice
	xdef __close_periphdevice

;**************************************************************************

_open_periphdevice:
__open_periphdevice:
_close_periphdevice:
__close_periphdevice:

	ret

;**************************************************************************
