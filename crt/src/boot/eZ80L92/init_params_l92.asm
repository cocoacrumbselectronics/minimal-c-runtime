;*****************************************************************************
; init_params_l92.asm
;
; minimum eZ80L92 initialization
;*****************************************************************************
; Copyright (C) 2005 by ZiLOG, Inc.  All Rights Reserved.
;*****************************************************************************

        include "ez80l92.inc"

;**************************************************************************
; Global Symbols Imported
;**************************************************************************
        
        xref __stack
        xref __init_default_vectors
        xref __c_startup
        xref __cstartup
        xref _main
        xref __CS0_LBR_INIT_PARAM
        xref __CS0_UBR_INIT_PARAM
        xref __CS0_CTL_INIT_PARAM
        xref __CS1_LBR_INIT_PARAM
        xref __CS1_UBR_INIT_PARAM
        xref __CS1_CTL_INIT_PARAM
        xref __CS2_LBR_INIT_PARAM
        xref __CS2_UBR_INIT_PARAM
        xref __CS2_CTL_INIT_PARAM
        xref __CS3_LBR_INIT_PARAM
        xref __CS3_UBR_INIT_PARAM
        xref __CS3_CTL_INIT_PARAM
        xref __CS0_BMC_INIT_PARAM
        xref __CS1_BMC_INIT_PARAM
        xref __CS2_BMC_INIT_PARAM
        xref __CS3_BMC_INIT_PARAM

;**************************************************************************
; Global Symbols Exported
;**************************************************************************

        xdef __init
        xdef _abort
        xdef __exit
        xdef _exit

;*****************************************************************************
; Startup code
        ; DEFINE .STARTUP, SPACE = ROM
        ; SEGMENT .STARTUP
        ; .ASSUME ADL = 1
	.section	.startup, "ax", @progbits
	.assume	    adl = 1
	.file	    "init_params_l92.asm"
	.section	.startup, "ax", @progbits
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Minimum default initialization
__init:
    ; disable internal peripheral interrupt sources
    ; -- this will help during a RAM debug session --
    ld a, 0xFF
    out0 (PB_DDR), a         ; GPIO
    out0 (PC_DDR), a         ;
    out0 (PD_DDR), a         ;
    ld a, 0x00
    out0 (PB_ALT1), a        ;
    out0 (PC_ALT1), a        ;
    out0 (PD_ALT1), a        ;
    out0 (PB_ALT2), a        ;
    out0 (PC_ALT2), a        ;
    out0 (PD_ALT2), a        ;
    out0 (TMR0_CTL), a       ; timers
    out0 (TMR1_CTL), a       ;
    out0 (TMR2_CTL), a       ;
    out0 (TMR3_CTL), a       ;
    out0 (TMR4_CTL), a       ;
    out0 (TMR5_CTL), a       ;
    out0 (UART0_IER), a      ; UARTs
    out0 (UART1_IER), a      ;
    out0 (I2C_CTL), a        ; I2C
    ld a, 0x04
    out0 (SPI_CTL), a        ; SPI
    in0 a, (RTC_CTRL)        ; RTC, Writing to the RTC_CTRL register also
    and a, 0xBE               ;      resets the RTC count prescaler allowing
    out0 (RTC_CTRL), a       ;      the RTC to be synchronized to another
                             ;      time source. 

    ; configure memory
    ld a, __CS0_LBR_INIT_PARAM
    out0 (CS0_LBR), a
    ld a, __CS0_UBR_INIT_PARAM
    out0 (CS0_UBR), a
    ld a, __CS0_BMC_INIT_PARAM
    out0 (CS0_BMC), a
    ld a, __CS0_CTL_INIT_PARAM
    out0 (CS0_CTL), a

    ld a, __CS1_LBR_INIT_PARAM
    out0 (CS1_LBR), a
    ld a, __CS1_UBR_INIT_PARAM
    out0 (CS1_UBR), a
    ld a, __CS1_BMC_INIT_PARAM
    out0 (CS1_BMC), a
    ld a, __CS1_CTL_INIT_PARAM
    out0 (CS1_CTL), a

    ld a, __CS2_LBR_INIT_PARAM
    out0 (CS2_LBR), a
    ld a, __CS2_UBR_INIT_PARAM
    out0 (CS2_UBR), a
    ld a, __CS2_BMC_INIT_PARAM
    out0 (CS2_BMC), a
    ld a, __CS2_CTL_INIT_PARAM
    out0 (CS2_CTL), a

    ld a, __CS3_LBR_INIT_PARAM
    out0 (CS3_LBR), a
    ld a, __CS3_UBR_INIT_PARAM
    out0 (CS3_UBR), a
    ld a, __CS3_BMC_INIT_PARAM
    out0 (CS3_BMC), a
    ld a, __CS3_CTL_INIT_PARAM
    out0 (CS3_CTL), a

    ; setup Stack Pointer
    ld sp, __stack

    ; initialize the interrupt vector table
    call __init_default_vectors

    ; start application
    ld a, __cstartup
    or a, a
    jr z, __no_cstartup
    call __c_startup

__no_cstartup:
    ;--------------------------------------------------
    ; Initialize the peripheral devices

        xref __open_periphdevice

    call __open_periphdevice

    ;---------------------------------------------
    ; prepare to go to the main system rountine
    ld hl, 0                   ; hl = NULL
    push hl                    ; argv[0] = NULL
    ld ix, 0
    add ix, sp                 ; ix = &argv[0]
    push ix                    ; &argv[0]
    pop hl
    ld de, 0                   ; argc = 0
    call _main                 ; int main(int argc, char *argv[]))
    pop de                     ; clean the stack

__exit:
_exit:
_abort:
    ;--------------------------------------------------
    ; Close the peripheral devices

        xref __close_periphdevice

    call __close_periphdevice

    jr $                ; if we return from main loop forever here

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
_SysClkFreq:
        ; DL _SYS_CLK_FREQ
        d32 _SYS_CLK_FREQ


        xref _SYS_CLK_FREQ
        xdef _SysClkFreq

        END
